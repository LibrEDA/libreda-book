# libreda-book

The ultimate guide through LibrEDA.

Start reading [directly in this repository](./src/SUMMARY.md) or find a possibly older version [online here](https://libreda.org/book).

This book is written in markdown.

Show the book with `mdbook` as follows:

```
cargo install mdbook

git clone [this repo]
cd [this repo]
mdbook serve

# Open displayed link in browser.
```
