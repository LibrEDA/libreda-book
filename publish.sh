#!/bin/bash

set -e

#cargo doc --workspace --no-deps

REPO=git@codeberg.org:LibrEDA/pages.git
COMMIT_MESSAGE="Update libreda-book."
TMP=$(mktemp -d)

function cleanup() {
    rm -rf $TMP
}

trap cleanup EXIT

git -C $TMP clone $REPO

cp -r ./book/ $TMP/pages/

pushd $TMP/pages/

git add book
git status
echo "Commit? (Enter=Yes, Ctrl-C=Abort)"
read
git commit -m "$COMMIT_MESSAGE"
git status
echo "Push? (Enter=Yes, Ctrl-C=Abort)"
read
echo "pushing in 2 seconds..."
sleep 2
git push
popd
