# Fused Netlist & Layout

[Netlists](./netlist.md) and [layouts](./layout.md) are just different representations of a circuit. LibrEDA reflects
this with the netlist and layout traits which build on top a hierarchical structure. Having both of this views
is not sufficient though. For many purposes it is necessary to know the links between netlist and layout. For example
it is helpful to know which layout shape belongs to which net or to know where the physical geometry of a pin is located.
For this reason LibrEDA uses the `L2NBase` and `L2NEdit` traits. `L2N` means layout-to-netlist. The both traits
extend the layout and netlist traits with the most fundamental links. This includes
* The layout shapes which belong to a pin.
* The layout shapes which belong to a net.
* The net which is connected to a layout shape.
* The pin which is made by a layout shape.