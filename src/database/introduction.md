# Database

'Database' is an often used term around EDA software. It basically refers to datastructures
which represent the chip during the design flow.

Most typically the following things need to be covered by the database:

* Netlist: An abstract graph-like (actually hyper-graph) representation of circuits.
* Layout: A two-dimensional representation of the actual circuit geometries.
* Logic: The abstract logical behaviour of circuit components, usually of the standard-cells.
* Timing: A representation of the signal delay when it passes through a circuit.
* Hierarchy: Usually the above structures are put into a hierarchy. For example a circuit can
be composed of other circuits.
* Technology data: The fabrication technology puts constraints on designs. This constraints must be represented somehow.
Often LEF/DEF files are used. In principle this can also be covered by the database.

## Database access traits

The [LibrEDA database](https://codeberg.org/libreda/libreda-db) has some important ideas at it's core:

Instead of putting a data *structure* at the core, the most important part in the LibrEDA DB are
*trait* definitions (similar to interfaces in other languages) that specify *how* certain structures can be accessed and modified.
In other words: Instead of telling how the data structures look, LibrEDA DB defines how they can be used.
Of course there is a reference data structure which implements this behaviours.

This abstraction has certain advantages:

* Algorithms are agnostic of the underlying netlist & layout representation in memory.
* The underlying data structures can be easily swapped.
* There can be different implementations in co-existence.
* It is possible to create wrappers and modifier structures that for example allow to record
database transactions and can undo them when needed.
* Instead of passing around pointers and references, the API is based on IDs. This simplifies binding to other languages.

## Views

The database traits are structured to provide certain *views* of a chip:

* Cell hierarchy: Composition relationship between the cells.
* Netlist: Introduces electrical connectivity with pins and nets.
* Layout: Introduces spacial layout and layered geometry. This includes position and orientation of cells and geometries on each of the layers.
* Fused netlist & layout: This brings the layout and netlist into relation. 
Nets and pins can be linked to geometric shapes etc.

The mentioned views are discussed in more detail in the following sections.