# Reference Access Pattern

*Syntactic sugar for read-only access to netlists, layouts, hierarchies.*

The basic traits for hierarchy, netlist and layout access are based on object IDs - handles which can 
cheaply be cloned. In the implementation this could be for instance integer numbers.
Working with IDs also has disadvantages such as non-object-style syntax.
For example to get all pins of a cell one might want to write `cell.each_pin()` instead of making the 
detour over `chip.each_pin(&cell)`.
This is where 'reference access' wrappers come into play. They wrap the object ID together with an immutable
reference to the base data-structure (a `HierarchyBase`, `NetlistBase`, `LayoutBase`, ... trait).
The wrapper data type now behaves much like an object with associated function. In the background this all 
maps back to the trait implementation with object IDs. Therefore the data-type implementing such traits needs 
only to provide ID based access and gets object-like access for free.
This access method is currently limited to read-only access. Good use cases are code pieces
which do not modify anything but for example analyze a netlist.