# Layout

'Layout' refers to the multi-layer geometrical shapes which describe the physical circuit.

Cells may contain geometrical shapes on different layers. Instances of cells
don't contain any shapes because they are 'clones' of their template cell.
However, instances have a location, rotation and magnification (rarely used)
relative to their parent cell.

Following the spirit of the hierarchy traits, LibrEDA also defines traits for
accessing and editing layout information: `LayoutBase` and `LayoutEdit`.
`LayoutBase` also defines ID types for layout objects (`Coord`, `LayerId`, `ShapeId`).
`Coord` specifies the actual data type used for coordinates in the euclidean plane.
For most applications this will be an `i32`.
`LayerId` is a handle which points to a certain layer. Layers can be referenced also by
name or `(index, datatype)` tuples as used in GDSII or Oasis file formats. 
However, names and layer numbers first need to be converted into a `LayerId` using
methods like `find_layer()` or `layer_by_name()`.

Geometrical shapes are based on the `iron-shapes` crate and further documented in [Euclidean Geometry](../geometry/euclidean_geometry.md).