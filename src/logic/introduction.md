# Logic

Place and route tools at some point need to deal with the logic behaviour
of components (e.g. standard-cells). Especially when the netlist is changed
to reach timing constraints the algorithm must somehow understand the logic
behaviour of the circuit in order to preserve it.
An other example is timing-analysis where delays of a gate depend on the values
of other inputs. Somehow the time-analysis algorithm needs to be able to compute this values
to choose good estimates of the delays.

There are different types of logic commonly used in EDA software:

* Two-valued logic ([boolean logic](https://en.wikipedia.org/wiki/Boolean_algebra)) with values `LOW` and `HIGH`.
* [Three-valued logic](https://en.wikipedia.org/wiki/Three-valued_logic) with values `LOW`, `HIGH` and `UNKNOWN`.
* 4-valued logic ([IEEE 1364](https://en.wikipedia.org/wiki/IEEE_1364)), with values `LOW`, `HIGH`, `UNKNOWN`, `HIGH_IMPEDANCE`
* 5-valued logic ([D-algebra](https://technobyte.org/d-algorithm-combinational-atpg-in-dft-vlsi/)), with values `0`, `1`, `D`, `D'`, `X`.
* 9-valued logic ([IEEE 1164](https://en.wikipedia.org/wiki/IEEE_1164), less likely to be used in physical synthesis)

An example for usage of a 5-valued logic are [automated test-pattern generators (ATPG)](../design_for_test/test_pattern_generator.md).
The 'D-algorithm' uses the 'D-algebra' to understand how manufacturing errors can be
detected in a circuit.

## TBD

* To allow useful abstractions `libreda-logic` defines traits for dealing with logic.