# Legalization

Legalization transforms an approximate *global* placement solution into a legal placement solution. 
This means in practice: Remove overlaps between standard-cells/macro-blocks and place standard-cells on rows.

There are possibly constraints and optimization criteria, for example minimization of wiring length.

## Example implementations

The following repositories contain legalization engines which can be used as inspiration for other implementations:

* [tetris-legalizer](https://codeberg.org/libreda/tetris-legalizer)