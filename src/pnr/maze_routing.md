# Maze Routing

'Maze routing' is an approach for finding shortest paths on a grid. The algorithm
is very similar to Dijkstra's shortest-path algorithm.
Horizontal and vertical tracks form a *routing grid* graph as shown in the illustrations below. 
Routing between two graph nodes happens by propagating a 'wave' from the *source* node.
In the beginning, all nodes are marked as *not visited*, except the *source node*. It is marked as visited
with distance 0 to the *source node*.
Iteratively, the closest neighbours of *visited* nodes are processed.


![](./img/maze_routing/steps/graph_routing_example_1_06.svg)
![](./img/maze_routing/steps/graph_routing_example_1_07.svg)
![](./img/maze_routing/steps/graph_routing_example_1_08.svg)
![](./img/maze_routing/steps/graph_routing_example_1_09.svg)
![](./img/maze_routing/steps/graph_routing_example_1_10.svg)