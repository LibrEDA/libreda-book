# Physical Synthesis - Place & Route

Physical synthesis is the process of converting a gate-level netlist into a physical chip layout which
satisfies certain constraints.
The layouts of the gates and their timing behaviour must be already given in a library. Usually
the layouts are stored in a LEF library or separate GDS/OASIS files. The behaviour (logical and timing)
is often described in 'liberty' files.

The following sections will cover the most important place & route steps and relate them to the relevant parts
of the LibrEDA framework.

## General idea of LibrEDA
The LibrEDA framework does not implement any place & route algorithms. But it provides fundamental data structures
and interfaces. Place & route algorithms are developed as separate projects. As long as they follow the 
core ideas of LibrEDA they can easily be integrated with each other.

## Example place & route flow

* [libreda-examples](https://codeberg.org/libreda/libreda-examples) implements an example place & route flow.
The program assembles parts of the LibrEDA framework into a stand-alone tool.

## References
* Electronic Design Automation: Synthesis, Verification, and Test (ISBN 9780123743640)
    * Some chapters can be found online:
    * http://cc.ee.ntu.edu.tw/~ywchang/Courses/PD_Source/EDA_Chapter1.pdf
    * http://cc.ee.ntu.edu.tw/~ywchang/Courses/PD_Source/EDA_floorplanning.pdf
    * http://cc.ee.ntu.edu.tw/~ywchang/Courses/PD_Source/EDA_placement.pdf
    * http://cc.ee.ntu.edu.tw/~ywchang/Courses/PD_Source/EDA_routing.pdf
    * Electronic Design Automation, Synthesis of clock and power/ground networks, (DOI: 10.1016/b978-0-12-374364-0.50020-5)