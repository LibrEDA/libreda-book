# Buffer and Tie-Cell Insertion

A netlist which comes from a logic synthesizer is often not fully suitable to be implemented as it is on a chip.
Logic synthesizers such as Yosys are not aware of the placement and hence cannot do everything optimally.
For example nets with constant values (0 or 1) must be driven from so-called 'tie-cells'. A tie-cell has just a constant
output signal, either 0 or 1. To minimize the required wiring, it is much better to insert such tie-cells once it is known
where they are needed. For that reason, synthesizers might not put tie-cells into the netlist at all but just use logic 
constants `0` and `1`.

Another example are high-fanout nets and buffer trees. Often, so many cells are attached to a net such that
the driver cell will be too weak and too slow. Typical techniques to solve this problem are
* buffer insertion between the driver cell and the signal sinks
* replication of the driver cell

Consider for example this setup with a `driver` and four sinks `a`, `b`, `c`, and `d`.
Since the driver is too weak, two buffers are inserted do then drive the sinks.
```txt
driver           c
                     d


              buf2


     buf1
 a


   b

```

Logically, it does not matter which of the sinks is attached to which buffer. But it matters once the circuit
is implemented.
When the placement of the cells is known, it is easy to see that `a` and `b` are best attached to `buf1` and the other
sinks to `buf2`:
```txt
driver           c
     +-+---+     ^   d
       |   |     |   ^
       |   |     +   |
       |   +->buf2+--+
       |
       v
     buf1
 a<---+
      |
      |
   b<-+
```

Without knowing the placement it is likely that a much worse solution would be picked. For example driving sinks `a`
and `d` with `buf1` results in much more wiring:
```txt
driver           c
     +-+---+     ^   d
       |   |     |   ^
       |   |     +   |
       |   +->buf2   |
       |         +   |
       v         |   |
     buf1        |   |
 a<---+--------------+
                 |
                 |
   b<------------+
```

Tie-cells can be regarded as a type of buffer of a constant signal. Because the signal is constant they can easily
be replicated without the need of wiring between them. But in order to share tie-cells among sinks which are close together
it is necessary to know the global placement.

## Implementations

* [Tie-cell insertion](https://codeberg.org/LibrEDA/libreda-pnr/src/branch/main/src/util/tie_cell_insertion.rs)
* [Buffer-tree engine](https://codeberg.org/LibrEDA/arboreus-cts)
