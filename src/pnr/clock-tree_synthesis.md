# Clock-tree synthesis (CTS)

Clock-tree synthesis is the art of distributing clock and reset signals to the circuit components which need them.
There are many requirements and many approaches.

## H-tree
TODO

## Example implementations
The [arboreus-cts](https:://codeberg.org/libreda/arboreus-cts) crate implements very simple algorithms for 
creating buffer trees. This is also useful for rebuffering of large-fanout nets and can be used for generating
clock and reset trees - more for demonstration purposes though. There's no timing optimization involved yet.