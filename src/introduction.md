
# License Notice

Permission is granted to copy, distribute and/or modify this
document under the terms of the GNU Free Documentation License,
Version 1.3 or any later version published by the Free Software
Foundation; with the Invariant Sections being just "License Notice",
with no Front-Cover Texts, and with no Back-Cover
Texts.  A copy of the license is included in the section
entitled "GNU Free Documentation License".

## Source code

*The original source code of this book can be found on [codeberg](https://codeberg.org/LibrEDA/libreda-book).*


# Introduction

## Physical Design of Silicon Chips
Physical design or physical synthesis starts with a description of the chip on the gate level
and turns that description step-by-step into a spacial representation that corresponds almost
one-to-one to the piece of silicon that should be manufactured.
This book covers the synthesis steps which come after *logic synthesis* and *technology mapping*.

## For who is this book?
This book is for everybody interested in building software for the physical design of silicon chips.
In order to build such software it is helpful to have a basic understanding of how silicon chips
work and how digital circuits are designed and synthesized to gate-level netlists.
There's no need to know much about high-level aspects such as architectures of digital circuits.
This book deals with circuits mostly on the gate-level (standard-cells) or macro-level (e.g. memory block).

## Prerequisites

### Digital Circuits
A basic understanding of digital circuits is necessary. This includes how digital logic gates behave
and how roughly they are implemented on silicon with transistors. Also it is important to be familiar
with the physical structure of a chip (there's a silicon substrate which contains the transistors, 
on top of that come multiple metalized layers that are used to form wires, via layers connect between 
the metal layers).

### Rust
LibrEDA is written in Rust. It is definitely helpful to have a basic understanding of the language.
However, the examples showed here are probably understandable by people familiar with C-like languages.

### Installing LibrEDA
See [libreda.org](https://libreda.org).

### Installing KLayout
KLayout is the recommended layout viewer. Any other layout viewer that can open OASIS files
can be used though.

See [klayout.org](https://klayout.org)

## LibrEDA

This books supports most topics with examples from an open-source implementation of the discussed
datastructures and algorithms.
There's a few open-source EDA tools, but since this book is written along the LibrEDA project it mainly focues on that.

From [libreda.org](https://libreda.org):

*LibrEDA is a young libre-software framework for the physical design of silicon chips and tries to simplify the development of place and route tools. 
A strong motivation is democratization fo silicon technology by making ASIC toolchains easily accessible for research, education and hobbyists. 
Today's society and especially the internet stand on integrated electronic circuits. 
Practically all chips that run the internet today are proprietary and closed-source pieces of hardware. This is a problem for transparency, trust, security and digital sovereignty.
To build open-source alternatives it is important to have open-source Electronic Design Automation (EDA) software. Since the underlying optimization problems are not only computationally complex but also complicated it is necessary to create a collaborative ecosystem. 
The framework approach of LibrEDA is an attempt to go into this direction.*

The following chapters are a higher level documentation of LibrEDA that often also applies to other tools.
Lower-level documentation can be found in the actual software repositories of LibrEDA.

### Idea and architecture

In contrast to other tools mentioned here, LibrEDA does not aim to be an end-user tool but a framework for creating such.
LibrEDA has a focus on modularity. Major components such as place & route tools should be developed as separate Rust *crates*.
They can then easily be integrated if they follow the core concepts of LibrEDA. This architecture should make it easier
to cooperate on tool development.
LibrEDA also defines fundamental structures for the representation of netlist and layouts, called [*data-base*](./database/introduction.md).
The approach is different from many other tools. Instead of having a set of data-structures which every component must 
use, LibrEDA defines interfaces. The interfaces (Rust *traits*) define how such data-structures can be accessed and modified.
This approach introduces more flexibility. Place & route engines build up on those interfaces are agnostic of the actual
implementation of the data-base. This makes it possible to provide different implementations of data-bases consistently. 
Also it allows to build wrappers around data-base structures. For example this could be a in-memory flattened view of a 
hierarchical structure or a wrapper which allows to *undo* operations.

## Other Free and Open-Source EDA Tools

Here's an incomplete list of other open-source Tools. Some of them are very helpful for working through this book.
Especially KLayout is recommended to inspect generated layouts because at the time of writing LibrEDA
does not provide a graphical user interface.

* [KLayout](https://klayout.org) is a **must** - KLayout started as a GDSII layout viewer.
In the meantime it grew considerably:
    * It is a versatile and scriptable layout editor with an active community
    * It can do design rule checks (DRC).
    * It can do layout versus schematic (LVS) checks.
* [Coriolis2]() is one of the more mature open-source place & route tools. It is developed in Paris at Sorbonne University.
* [Magic/QFlow]()
* [OpenRoad]() is an effort DARPA and the US electronic resurgence initiative to boost EDA tool innovations
by making use of the efficient nature of open-source software and by pulling-in universities across the globe.
