# File Formats

This section currently contains mainly links to implementations of named file formats.

## Netlists

### Verilog

* [libreda-structural-verilog](https://codeberg.org/LibrEDA/libreda-structural-verilog) - Verilog parser & writer
for the structural netlist format used by Yosys. Behavioral verilog is *not* supported - only gate-level netlists. Netlists
can be hierarchical though.

### DEF
DEF can also encode netlists. At time of writing this is not supported yet though.
* [libreda-lefdef](https://codeberg.org/LibrEDA/libreda-lefdef) - DEF parser & writer

### Layouts

### GDSII
Probably the most famous file format for chip layouts. GDSII is unfortunately not nicely defined and has many
pitfalls. There is no implementation yet to be mentioned here. The recommended way is to use OASIS
as layout input and output format. OASIS can be reliably converted to and from GDSII using [KLayout](https://klayout.org).

### OASIS
OASIS is a good successor of GDSII. It is quite well defined and much more space efficient than GDSII, hence also
faster to load and write.

* [libreda-oasis](https://codeberg.org/LibrEDA/libreda-oasis) OASIS parser & writer

### LEF/DEF

* [libreda-lefdef](https://codeberg.org/LibrEDA/libreda-lefdef) - LEF/DEF parser & writer

## Library

### Liberty
Liberty encodes library data such as timing behaviour of cells.

* [liberty-io](https://codeberg.org/LibrEDA/liberty-io) - Liberty parser, writer and tools.