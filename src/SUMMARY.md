# Summary

[Introduction](./introduction.md)

- [Database](./database/introduction.md)
    - [Hierarchy](./database/hierarchy.md)
    - [Netlist](./database/netlist.md)
    - [Layout](./database/layout.md)
    - [Fused Netlist & Layout](./database/fused_netlist_layout.md)
    - [Reference Access](./database/reference_access_pattern.md)

- [Geometry]()
    - [Euclidean Geometry](./geometry/euclidean_geometry.md)
    - [Boolean Operations]()
    - [Region Queries]()
    
- [Logic](./logic/introduction.md)
    - [Boolean functions]()
    - [Truth-tables]()
    - [Boolean networks]()
    
# PDKs

- [Introduction]()
    - [OpenSource PDKs]()
    
# Library

- [Introduction]()
    - [Generate Standard-Cells]()
    - [Generate Memory-Macros]()
    
# I/O & File Formats

- [File formats](./file_formats/file_formats.md)
    - [LEF/DEF]()
    - [OASIS]()
    - [Verilog]()
    - [Liberty]()

# Physical Synthesis

- [Introduction](./pnr/introduction.md)
- [Floorplan]()
    - [Power Routing]()
    - [Well Taps]()
    - [IO Pads]()
- [Placement](./pnr/placement.md)
    - [Global Placement](./pnr/global_placement.md)
    - [Legalization](./pnr/legalization.md)
- [Buffer & tie-cell insertion](./pnr/buffer_tiecell_insertion.md)
- [Routing](./pnr/routing.md)
    - [Maze Routing](./pnr/maze_routing.md)
    - [Conflict Resolution]()
        - [Rip-up & Reroute]()
        - [Negotiation Based Algorithms]()
    - [Hierarchical Routing]()
        - [Global Routing]()
        - [Resource Estimation]()
        - [Multilevel Full-Chip Routing](./pnr/multi_level_routing.md)
    - [Line-Probe Algorithms](./pnr/line_probe_routing.md)
- [Clock-Tree Synthesis](./pnr/clock-tree_synthesis.md)
- [Timing Driven Place & Route]()
    - [Static Timing Analysis]()
        - [Wire Delay Estimation]()
    - [Timing Driven Placement]()
    - [Hold Time Fixing]()
    - [Timing Optimization Strategies]()

# Verification

- [Netlist Extraction]()
- [Netlist Comparison]()

# Design For Test (DFT)

- [Scan-Chain Insertion](./design_for_test/scan_chain_insertion.md)
- [Automated Test-Pattern Generation](./design_for_test/test_pattern_generator.md)

# Appendix

[GNU Free Documentation Licence](./gnu_free_documentation_license_1.3.md)
