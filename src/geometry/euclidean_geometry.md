# Euclidean Geometry

Euclidean geometry - geometry in the two dimensional plane - is important for 
handling chip layouts. A separate crate [`iron-shapes`](https://codeberg.org/libreda/iron-shapes) is devoted to this task.
The crate defines geometric data types such as points, vectors, polygons and transformations.
They are used for everything related to layouts.